<?php

/**
 * Get clean HTML for each template and save it to files for comparison
 */

// Get list of templates
$host = $_SERVER['HTTP_HOST'];
$dom = new DOMdocument();
$dom->loadHTML(file_get_contents("http://$host/templates/"));

// Find the links we want
$finder = new DomXPath($dom);
$links = $finder->query("//*[contains(@href, 'clean=1')]");

// Get a list of clean HTML urls
$urls = array();

for ($i = 0; $i < $links->length; ++$i) {
  $link = $links->item($i);
  for ($a = 0; $a < $link->attributes->length; ++$a) {
    $attr = $link->attributes->item($a);
    if($attr->name == 'href') {
      array_push($urls, $attr->nodeValue);
    }
  }
}

// Save each clean HTML page to a file based on template name
foreach($urls as $url) {
  $template = str_replace(array('&', '=', '/templates/index.php?page-', '-clean-1', '&'), array('-', '-', ''), $url);
  $html = file_get_contents("http://$host$url");
  file_put_contents("../$template.html", $html);
  echo "<h3>Done saving generated HTML for $template</h3>";
}