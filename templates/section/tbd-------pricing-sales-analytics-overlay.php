<section class="qlik-pricing-overlay no-padding">
  <div class="container pricing-sales-analytics-overlay">
    <div class="v-middle-inner">
      <div class="v-middle">
        <div class="main-wrapper">
          <div class="content-wrapper">
            <div class="intro-block text-center">
              <h3>User Type Comparison Chart</h3>
              <p>What type of user are you?</p>
            </div>
            <div class="column-wrapper">
              <div class="col-two">
                <div class="overlay-title bg-primary">
                  <h5>Analyzer User</h5>
                </div>
                <div class="feature-details bg-light-gray">
                  <ul>
                    <li class="dot-line"><p>Features Include:</p></li>
                    <li><p>Standard navigation and access to Hub (via extranet included)
                      <p></li>
                    <li><p>Create bookmarks and stories
                      <p></li>
                    <li><p>Print object/stories/sheets
                      <p></li>
                    <li><p>Export data from an object to XLS
                      <p></li>
                    <li><p>Authenticates mobile
                      <p></li>
                  </ul>
                  <a href="#contact-us" class="btn-default btn-primary">Contact Us to Order</a>
                </div>
              </div>
              <div class="col-two">
                <div class="overlay-title bg-light-blue">
                  <h5>Professional User</h5>
                </div>
                <div class="feature-details bg-light-gray">
                  <ul>
                    <li class="dot-line"><p>All the features of Analyzer plus:
                      <p></li>
                    <li><p>Access to My Work
                      <p></li>
                    <li><p>Create apps and sheets
                      <p></li>
                    <li><p>Editing capabilities
                      <p></li>
                    <li><p>Create and publish new applications
                      <p></li>
                    <li><p>Authenticates mobile and desktop
                      <p></li>
                    <li class="dot-line"><p>Full access to all features on the interface
                      <p></li>
                  </ul>
                  <a href="#contact-us" class="btn-default btn-primary">Contact Us to Order</a>
                </div>
              </div>
            </div>
          </div>
          <span class="close">
                <img src="/templates/dist/images/pricing-details/cross-sign.svg" alt="close button">
            </span>
        </div>
      </div>
    </div>
  </div>
</section>