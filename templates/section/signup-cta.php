<section class="signup-cta text-center">
  <div class="container">
    <div class="signup-content">
      <h2>Sign Up for a Free Trial or Demo</h2>
      <div class="cta-btn">
        <a href="http://demopharmasolution.sdgeagle.com" class="btn-default cta_launch_demo_top" target="_blank">Launch Demo</a>
        <a href="#trial" class="btn-two btn-default inner-link cta_free_trial">Get Free Trial</a>
      </div>
    </div>
  </div>
</section>