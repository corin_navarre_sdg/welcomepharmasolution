<section class="pricing-details">
  <div class="container">
    <div class="intro-block text-center">
      <h3>Eagle Pharma CRM® Pricing</h3>
    </div>

    <div class="column-wrapper">
      <div class="col-four">
        <div class="hover content-box">
          <div class="front bg-cashmere">
            <div class="content-text text-center">
              <h4>Custom</h4>
              <div class="sub-head">
                <p>Contact us for Pricing</p>
                <span>Billed based on deliverables</span>
              </div>
              <div class="pricing-text">
                <p>The sky is the limit here. This option is for companies interested in a custom solution that meets
                  their
                  specific needs. Everything from the environment to the applications can be customized.</p>
                <span>2 year min. contract</span>
              </div>
              <div class="price-link">
                <a href="" class="btn-transparent">More Info</a>
              </div>
            </div>
          </div>

          <div class="back bg-cashmere">
            <div class="content-text text-center">
              <h6>Any number of users and license types</h6>
              <div class="sub-head">
                <span>Any mix of Professional Analyzer Users Can include GeoAnalytics, Nprinting and even 3rd Party AI capabilities.</span>
              </div>
              <div class="pricing-text">
                <h6>Any amount of data</h6>
                <p>Bespoke solutions Advisory or Implementation services Custom training Any number of data sources</p>
              </div>
              <div class="price-link">
                <a href="/pricing.html" class="btn-transparent">Learn More</a>
              </div>
            </div>
            <span class="close">
              <img src="/templates/dist/images/pricing-details/cross-sign-white.svg" alt="close button">
            </span>
          </div>
        </div>
      </div>

      <div class="col-four power-block">
        <div class="hover content-box">
          <div class="front bg-primary">
            <div class="content-text text-center">
              <h4>Power</h4>
              <div class="sub-head">
                  <span>Most popular option</span>
                <h5>$99/Month</h5>
                <span>per User/Month Billed annually</span>
              </div>
              <div class="pricing-text">
                <p>Excellent option for advanced teams prefer to be empowered through self-service to create and modify
                  their applications on the fly. </p>
                <span>1 year min. contract</span>
              </div>
              <div class="price-link">
                <a href="" class="btn-transparent">More Info</a>
                <!--                <a href="" class="price-btn">More Info</a>-->
              </div>
            </div>
          </div>
          <div class="back bg-primary">
            <div class="content-text text-center">
              <h6>20 Users and Up</h6>
              <div class="sub-head">
                <p>Qlik Professional or Analyzer Users Includes GeoAnalytics Capabilities</p>
              </div>
              <div class="pricing-text">
                <h6>2GB Data</h6>
                <ul>
                  <li>Packaged Applications Enablement Services Instructor Led</li>
                  <li>Virtual Training 24/7 Support</li>
                </ul>
                <span>App personalization available at additional cost</span>
                <span>If the limit of 2GB of data is passed, there will be $5/user-month extra fee</span>
              </div>
              <div class="price-link">
                <a href="/pricing.html" class="btn-transparent">Learn More</a>
              </div>
            </div>
            <span class="close">
              <img src="/templates/dist/images/pricing-details/cross-sign-white.svg" alt="close button">
            </span>
          </div>
        </div>
      </div>

      <div class="col-four">
        <div class="hover content-box">
          <div class="front bg-cashmere">
            <div class="content-text text-center">
              <h4>Explorers</h4>
              <div class="sub-head">
                <h5>$75/Month</h5>
                <span>Per User/Month Billed annually</span>
              </div>
              <div class="pricing-text">
                <p>Good for teams that need dynamic access to information and prefer external support to manage changes
                  or
                  new requests.</p>
                <span>1 year min. contract</span>
              </div>
              <div class="price-link">
                <a href="" class="btn-transparent">More Info</a>
              </div>
            </div>
          </div>
          <div class="back bg-cashmere">
            <div class="content-text text-center">
              <h6>20 Users and Up</h6>
              <div class="sub-head">
                <p>Qlik Analyzer Users Includes GeoAnalytics Capabilities</p>
              </div>
              <div class="pricing-text">
                <h6>1GB Data</h6>
                <ul>
                  <li>Packaged Applications Quick start configuration</li>
                  <li>Training Videos 8-8 EST / 5 days Support</li>
                </ul>
                <span>App personalization available at additional cost</span>
                <span>If the limit of 2GB of data is passed, there will be $5/user-month extra fee</span>
              </div>
              <div class="price-link">
                <a href="/pricing.html" class="btn-transparent">Learn More</a>
              </div>
            </div>
            <span class="close">
              <img src="/templates/dist/images/pricing-details/cross-sign-white.svg" alt="close button">
            </span>
          </div>
        </div>
      </div>

      <div class="col-four">
        <div class="hover content-box">
          <div class="front bg-cashmere">
            <div class="content-text text-center">
              <h4>Free Trial</h4>
              <div class="sub-head">
                <span>One Month Free Trial</span>
                <p class="big-font">Free!</p>
                <span>Not Billed at all</span>
              </div>
              <div class="pricing-text">
                <p>Curious and need to see how visual data analytics can help? This is the package to start with.</p>
                <span>No Commitment!</span>
              </div>
              <div class="price-link">
                <!--                <a href="" class="price-btn">More Info</a>-->
                <a href="" class="btn-transparent">More Info</a>
              </div>
            </div>
          </div>
          <div class="back bg-cashmere">
            <div class="content-text text-center">
              <h6>Up to 5 Users</h6>
              <div class="sub-head">
                <p>Any type of user: Professional or Analyzer Includes GeoAnalytics Capabilities</p>
              </div>
              <div class="pricing-text">
                <h6>600MB Data</h6>
                <p>8-8 EST / 5 days Support</p>
              </div>
              <div class="price-link">
                <a href="/pricing.html" class="btn-transparent">Learn More</a>
              </div>
            </div>
            <span class="close">
              <img src="/templates/dist/images/pricing-details/cross-sign-white.svg" alt="close button">
            </span>
          </div>
        </div>
      </div>

    </div>
    <div class="redirect-btn">
        <a href="/pricing.html#pricing-details" class="btn-default cta_pricing_details">Pricing Details</a>
      <a href="/pricing.html#compare" class="btn-default cta_compare_packages">Compare Packages</a>
    </div>
  </div>
</section>