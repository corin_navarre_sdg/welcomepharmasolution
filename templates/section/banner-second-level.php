<section class="banner-second-level no-padding">
  <div class="bg-img">
    <img src="/templates/dist/images/pricing-details/banner-second-level-bg.jpg" alt="banner second level">
  </div>
  <div class="v-middle-wrapper">
    <div class="container v-middle-inner">
      <div class="v-middle">
        <h1>Pricing Details</h1>
        <h6>The following section describes in detail the pricing details for each package</h6>
        <a href="#trial" class="btn-default btn-transparent">Sign Up for a Free Trial</a>
      </div>
    </div>
  </div>
</section>
