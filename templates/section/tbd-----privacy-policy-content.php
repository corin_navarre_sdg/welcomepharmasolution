<section class="privacy-policy-content">
  <div class="container">
    <div class="intro-block text-center">
      <h5>ONLINE PRIVACY POLICY AGREEMENT</h5>
    </div>

    <div class="policy-details">
      <p class="policy-text">SDG Group, (SDG), is committed to keeping any and all personal information collected of
        those individuals that
        visit our website and make use of our online facilities and services accurate, confidential, secure and private.
        Our privacy policy has been designed and created to ensure those affiliated with SDG Group of our commitment and
        realization of our obligation not only to meet but to exceed most existing privacy standards. </p>
      <p class="policy-text"><strong><em>THEREFORE,</em></strong> this Privacy Policy Agreement shall apply to SDG Group
        , and thus it shall govern
        any and all data collection and usage thereof. Through the use of www.sdggroup.com you are herein consenting to
        the following data procedures expressed within this agreement. </p>
      <div class="agreement-info">
        <h6>Collection of Information</h6>
        <p>This website collects various types of information, such as:</p>
        <ul>
          <li>
            <p>Voluntarily provided information which may include your name, address, email address, billing and/or
              credit card information etc., which may be used when you purchase products and/or services and to deliver
              the services you have requested.</p>
          </li>
          <li>
            <p>Corporate data voluntarily loaded into SDG’s Eagle Solutions for the purpose of analytics. </p>
          </li>
          <li>
            <p>Information automatically collected when visiting our website, which may include cookies, third party
              tracking technologies and server logs. </p>
          </li>
        </ul>
        <p class="policy-text">Please rest assured that this site shall only collect personal information that you
          knowingly and willingly provide by way of surveys, completed membership forms, upload into Eagle solutions and
          emails. It is the intent of this site to use personal information only for the purpose for which it was
          requested and any additional uses specifically provided on this site. </p>

        <p class="policy-text">We may also gather information about the type of browser you are using, IP address or type
          of operating system to assist us in providing and maintaining superior quality service.</p>

        <p class="policy-text">It is highly recommended and suggested that you review the privacy policies and statements
          of any website you choose to use or frequent as a means to better understand the way in which other websites
          garner, make use of and share information collected. </p>
        <h6>Use of Information Collected</h6>
        <p class="policy-text">SDG Group may collect and may make use of personal information to assist in the operation
          of our website and to ensure delivery of the services you need and request. At times, we may find it necessary
          to use personally identifiable information as a means to keep you informed of other possible products and/or
          services that may be available to you from www.sdggroup.com. SDG Group may also be in contact with you with
          regards to completing surveys and/or research questionnaires related to your opinion of current or potential
          future services that may be offered.</p>

        <p class="policy-text">SDG Group does not now, nor will it in the future, sell, rent or lease any of our customer
          lists and/or names to any third parties.</p>

        <p class="policy-text">SDG Group may feel it necessary, from time to time, to make contact with you on behalf of
          other external business partners with regards to a potential new offer which may be of interest to you. If you
          consent or show interest in presented offers, then, at that time, specific identifiable information, such as
          name, email address and/or telephone number, may be shared with the third party. </p>

        <p class="policy-text">SDG Group may deem it necessary to follow websites and/or pages that our users may
          frequent in an effort to gleam what types of services and/or products may be the most popular to customers or
          the general public.</p>

        <p class="policy-text">SDG Group may disclose your personal information, without prior notice to you, only if
          required to do so in accordance with applicable laws and/or in a good faith belief that such action is deemed
          necessary or is required in an effort to:</p>
        <ul>
          <li>
            <p>Remain in conformance with any decrees, laws and/or statutes or in an effort to comply with any process
              which may be served upon SDG Group and/or our website;</p>
          </li>
          <li>
            <p>Maintain, safeguard and/or preserve all the rights and/or property of SDG Group; and</p>
          </li>
          <li>
            <p>Perform under demanding conditions in an effort to safeguard the personal safety of users of
              www.sdggroup.com and/or the general public. </p>
          </li>
        </ul>

        <h6>Unsubscribe or Opt-Out</h6>
        <p class="policy-text">All users and/or visitors to our website have the option to discontinue receiving
          communication from us and/or reserve the right to discontinue receiving communications by way of email or
          newsletters. To discontinue or unsubscribe to our website please send an email that you wish to unsubscribe to
          contact.usa@sdggroup.com. If you wish to unsubscribe or opt-out from any third party websites, you must go to
          that specific website to unsubscribe and/or opt-out. </p>

        <h6>Links to Other Web Sites</h6>
        <p class="policy-text">Our website does contain links to affiliate and other websites. SDG Group does not claim
          nor accept responsibility for any privacy policies, practices and/or procedures of other such websites.
          Therefore, we encourage all users and visitors to be aware when they leave our website and to read the privacy
          statements of each and every website that collects personally identifiable information. The aforementioned
          Privacy Policy Agreement applies only and solely to the information collected by our website. </p>

        <h6>Security</h6>
        <p class="policy-text">SDG Group shall endeavor and shall take every precaution to maintain adequate physical,
          procedural and technical
          security with respect to our offices and information storage facilities so as to prevent any loss, misuse,
          unauthorized access, disclosure or modification of the user's personal information under our control.</p>

        <p class="policy-text">The company also uses Secure Socket Layer (SSL) for authentication and private
          communications in an effort to
          build users' trust and confidence in the internet and website use by providing simple and secure access and
          communication of credit card and personal information. </p>

        <h6>Changes to Privacy Policy Agreement</h6>
        <p class="policy-text">SDG Group reserves the right to update and/or change the terms of our privacy policy, and
          as such we will post those change to our website homepage at www.sdggroup.com, so that our users and/or
          visitors are always aware of the type of information we collect, how it will be used, and under what
          circumstances, if any, we may disclose such information. If at any point in time SDG Group decides to make use
          of any personally identifiable information on file, in a manner vastly different from that which was stated
          when this information was initially collected, the user or users shall be promptly notified by email. Users at
          that time shall have the option as to whether or not to permit the use of their information in this separate
          manner. </p>

        <h6>Acceptance of Terms</h6>
        <p class="policy-text">Through the use of this website, you are hereby accepting the terms and conditions
          stipulated within the aforementioned Privacy Policy Agreement. If you are not in agreement with our terms and
          conditions, then you should refrain from further use of our sites. In addition, your continued use of our
          website following the posting of any updates or changes to our terms and conditions shall mean that you are in
          agreement and acceptance of such changes. </p>

        <h6>How to Contact Us</h6>
        <p class="policy-text">If you have any questions or concerns regarding the Privacy Policy Agreement related to
          our website, please feel free to contact us at the following email, telephone number or mailing address.</p>

        <p class="policy-text"><span>Email: </span> contact.usa@sdggroup.com</p>

        <p class="policy-text"><span>Telephone Number: </span> 877-697-3487</p>

        <p class="policy-text">
          <span>Mailing Address:</span>
          <span class="address">SDG Group</span>
          <span class="address">100 Overlook Ctr., 2nd Floor</span>
          <span class="address">Princeton, New Jersey  08540</span>
        </p>
      </div>

    </div>
  </div>

</section>