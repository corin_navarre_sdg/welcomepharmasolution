<section class="col-three-block">
  <div class="container">
    <div class="intro-block text-center">
      <h3>You’re One Step Closer to <br>Enabling a Data-Driven Sales Force</h3>
    </div>
    <a href="#" class="video-block play-video" data-video=" https://www.youtube.com/embed/sYZ-7UG6UC8">
      <div class="bg-img">
        <div class="v-middle-inner">
          <div class="v-middle icon">
            <img src="/templates/dist/images/eagle.png" alt="eagle icon">
          </div>
        </div>
      </div>
      <div class="play-btn" >
        <img src="/templates/dist/images/play-button.png" alt="play button">
      </div>
    </a>
    <div class="column-wrapper">

      <div class="col-three content-box">
        <div class="content-head bg-primary">
          <h5>Improve HCP Engagement</h5>
        </div>
        <div class="content-text">
          <p>Reps gain a 360-degree view of Health Care Providers (HCPs) for a competitive advantage</p>
        </div>
      </div>

      <div class="col-three content-box">
        <div class="content-head bg-curious-blue">
          <h5>Increase Sales Efficiency</h5>
        </div>
        <div class="content-text">
          <p>Sales management and reps can easily leverage the Qlik® platform anywhere and anytime</p>
        </div>
      </div>

      <div class="col-three content-box">
        <div class="content-head bg-light-blue">
          <h5>Accelerate Sales</h5>
        </div>
        <div class="content-text">
          <p>Sales teams are empowered with access to data to drive real-time sales activities</p>
        </div>
      </div>

    </div>
  </div>
</section>
