<section class="pricing-overlay no-padding-bottom">
    <span class="close">
      <img src="/templates/dist/images/pricing-details/cross-sign.svg" alt="close button">
    </span>
  <?php include "banner-second-level.php"; ?>
  <section class="pricing-overlay-bg">
    <?php include "table-sales-analytics.php"; ?>
    <?php include "table-comparison-chart.php"; ?>
  </section>
  <?php include "signup-cta.php"; ?>
  <?php include "contact-us.php"; ?>
</section>