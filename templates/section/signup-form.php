<section class="signup-form text-center" ng-controller="formCtrl" id="trial">
  <div class="container">
    <div class="form-wrap">
      <!-- Sign up form start -->
      <div class="signup-form-inner">
        <div class="intro-block">
          <h3>Get a 30 Day Free Trial</h3>
        </div>
        <div class="column-wrapper">
          <div class="col-two bg-astronaut-blue form-view">
            <h4>Sign Up</h4>
            <form name="signupform" id="sign-up-form" method="post" netlify-honeypot="bot-field" netlify>
              <div class="field">
                <input type="text" name="first_name" placeholder="First Name" ng-model="signup.first_name"
                       ng-class="{ 'has-error' : signupform['first_name'].$error.required && (signupform['first_name'].$touched || submitted) }"
                       required>
                <span class="tooltiptext"
                      ng-show="signupform['first_name'].$error.required && (signupform['first_name'].$touched || submitted)">Please enter your first name</span>
              </div>
              <div class="field">
                <input type="text" name="last_name" placeholder="Last Name" ng-model="signup.last_name"
                       ng-class="{ 'has-error' : signupform['last_name'].$error.required && (signupform['last_name'].$touched || submitted) }"
                       required>
                <span class="tooltiptext"
                      ng-show="signupform['last_name'].$error.required && (signupform['last_name'].$touched || submitted)">Please enter your last name</span>
              </div>
              <div class="field">
                <input type="text" name="company_name" placeholder="Company Name" ng-model="signup.company_name"
                       ng-class="{ 'has-error' : signupform['company_name'].$error.required && (signupform['company_name'].$touched || submitted) }"
                       required>
                <span class="tooltiptext"
                      ng-show="signupform['company_name'].$error.required && (signupform['company_name'].$touched || submitted)">Please enter your company name</span>
              </div>
              <div class="field">
                <input name="email" type="email" ng-model="email" exempt-mail required placeholder="Work Email">
                <div class="error-msgs" ng-messages="signupform.email.$error">
                  <span class="tooltiptext" ng-message="workmail">Please, make sure to use your business email address to sign up</span>
                  <span class="tooltiptext" ng-message="validmail">Please enter a valid email address</span>
                  <span class="tooltiptext" ng-message="emailrequired">Please enter your email address</span>
                </div>
              </div>
              <div class="field">
                <input type="text" name="residence" placeholder="Country of Residence" ng-model="signup.residence"
                       ng-class="{ 'has-error' : signupform['residence'].$error.required && (signupform['residence'].$touched || submitted) }"
                       required>
                <span class="tooltiptext"
                      ng-show="signupform['residence'].$error.required && (signupform['residence'].$touched || submitted)">Please enter your residence</span>
              </div>
              <input type="number" name="phone_number" placeholder="Phone Number (Optional)">
              <input type="hidden" name="group_id" value="">
              <div class="checkbox field">
                  <div class="checkbox-inner">
                      <input type="checkbox" name="termsconditions" required ng-model="signup.termsconditions" ng-class="{ 'has-error' : signupform['termsconditions'].$error.required && (signupform['termsconditions'].$touched || submitted) }">
                      <label>I agree to all <a href="" class="terms-conditions-link">Terms & Conditions</a></label>
                  </div>
                <span class="tooltiptext"  ng-show="signupform['termsconditions'].$error.required && (signupform['termsconditions'].$touched || submitted)">Please check this box if you want to proceed</span>
              </div>
              <div class="field">
                <div class="g-recaptcha-err"></div>
<!--                <div class="g-recaptcha" data-sitekey="6LcWSnUUAAAAAMaA83N8Of0thlFJFT9wLI46q7fc"></div>-->
                               <div data-netlify-recaptcha data-callback="recaptcha_callback"></div>
              </div>
              <button type="submit" class="btn-default btn-two" ng-class="{'formerror' : signupform.$invalid }"
                      ng-click="submitted = true" ng-disabled="signupform.$invalid">Get the Trial
              </button>
            </form>
          </div>
          <div class="col-two bg-light-gray form-content v-middle-inner">
            <div class="form-details v-middle">
              <ul>
                <li class="list-1">Upload your own Veeva data and experience the power of SDG Group’s Eagle Pharma CRM®
                  solution.
                </li>
                <li class="list-2">Slice and dice your data visually and tell your own story based on your
                  analysis.
                </li>
                <li class="list-3">Leverage the power of advanced Qlik&reg;  algorithms to unhide insights in your
                  data.
                </li>
                <li class="list-4">Under the power of Qlik® GeoAnalytics and what it can do for your business.
                </li>
                <li class="list-5">Analyze data and capture new discoveries about your business anytime from your mobile
                  device.
                </li>
              </ul>
            </div>
          </div>


        </div>
      </div>
      <!-- Sign up form end -->

      <!-- Colleagues form start -->
      <div class="signup-form text-center colleagues-form thank-you-form" id="colleagues-form">
        <div class="intro-block">
          <h3>Thank You for Signing Up</h3>
        </div>
        <div class="column-wrapper">
          <div class="col-two form-view bg-astronaut-blue">
            <h4>Invite Colleagues</h4>
            <p>Enter the emails of the colleagues that you would like to invite to the free trial. All email
              addresses must
              have the same domain as yours.</p>
            <form name="invite_colleagues" id="invite_colleagues" method="post" netlify-honeypot="bot-field" netlify>
              <div class="field">
                <input name="email1" type="email" ng-model="email1" colleagues-mail required placeholder="Email 1">
                <div class="error-msgs" ng-messages="invite_colleagues.email1.$error">
                  <span class="tooltiptext" ng-message="workmail">Please, make sure to use your business email address to sign up</span>
                  <span class="tooltiptext" ng-message="validmail">Please enter a valid email address</span>
                  <span class="tooltiptext" ng-message="emailrequired">Please enter your email address</span>
                  <span class="tooltiptext" ng-message="businessmail">Please enter similar bussiness domain for email address</span>
                  <span class="tooltiptext" ng-message="emailmatch">Duplicate email address</span>
                </div>
              </div>
              <div class="field">
                <input name="email2" type="email" ng-model="email2" colleagues-mail  placeholder="Email 2">
                <div class="error-msgs" ng-messages="invite_colleagues.email2.$error">
                  <span class="tooltiptext" ng-message="workmail">Please, make sure to use your business email address to sign up</span>
                  <span class="tooltiptext" ng-message="validmail">Please enter a valid email address</span>
                  <span class="tooltiptext" ng-message="emailrequired">Please enter your email address</span>
                  <span class="tooltiptext" ng-message="businessmail">Please enter similar bussiness domain for email address</span>
                  <span class="tooltiptext" ng-message="emailmatch">Duplicate email address</span>
                </div>
              </div>
              <div class="field">
                <input name="email3" type="email" ng-model="email3" colleagues-mail  placeholder="Email 3">
                <div class="error-msgs" ng-messages="invite_colleagues.email3.$error">
                  <span class="tooltiptext" ng-message="workmail">Please, make sure to use your business email address to sign up</span>
                  <span class="tooltiptext" ng-message="validmail">Please enter a valid email address</span>
                  <span class="tooltiptext" ng-message="emailrequired">Please enter your email address</span>
                  <span class="tooltiptext" ng-message="businessmail">Please enter similar bussiness domain for email address</span>
                  <span class="tooltiptext" ng-message="emailmatch">Duplicate email address</span>
                </div>
              </div>
              <div class="field">
                <input name="email4" type="email" ng-model="email4" colleagues-mail  placeholder="Email 4">
                <div class="error-msgs" ng-messages="invite_colleagues.email4.$error">
                  <span class="tooltiptext" ng-message="workmail">Please, make sure to use your business email address to sign up</span>
                  <span class="tooltiptext" ng-message="validmail">Please enter a valid email address</span>
                  <span class="tooltiptext" ng-message="emailrequired">Please enter your email address</span>
                  <span class="tooltiptext" ng-message="businessmail">Please enter similar bussiness domain for email address</span>
                  <span class="tooltiptext" ng-message="emailmatch">Duplicate email address</span>
                </div>
              </div>
              <input type="hidden" name="group_id" value="">
              <div class="checkbox field">
                  <div class="checkbox-inner">
                <input type="checkbox" name="termsconditions" required colleagues-terms ng-model="termsconditions" >
                <label>I agree to all <a href="" class="terms-conditions-link">Terms & Conditions</a></label>
                  </div>
                  <div class="error-msgs" ng-messages="invite_colleagues.termsconditions.$error">
                  <span class="tooltiptext" ng-message="termsrequired">Please check this box if you want to proceed</span>
                  </div>
<!--                <span class="tooltiptext"  ng-show="invite_colleagues['termsconditions'].$error.required && (invite_colleagues['termsconditions'].$touched || submitted)">Please check this box if you want to proceed</span>-->
              </div>
              <button type="submit" class="btn-default btn-two" ng-class="{'formerror' : invite_colleagues.$invalid }"
                      ng-click="submitted = true" ng-disabled="invite_colleagues.$invalid">Invite Colleagues
              </button>
            </form>
          </div>
          <div class="col-two form-content bg-light-gray v-middle-inner">
            <div class="form-details v-middle">
              <p>You like it? Share it with yours peers and colleagues. Invite up to 4 of your colleagues to
                join your Free
                Trial and share all the benefits with them. Also, keep an eye on your inbox for an email
                with your Free
                Trial credentials and detailed instructions on how to access the environment.</p>
              <ul>
                <li class="list-1">Easily collaborate with others in your Organizations with powerful data
                  analytics.
                </li>
                <li class="list-2">Share insights, data discoveries and stories with your peers as a
                  Group.
                </li>
                <li class="list-3">Maximize the benefits of your trial with your team.</li>
                <li class="list-4">Standerlize the understanding of analytics and get consensus on business
                  much faster.
                </li>
              </ul>
            </div>
          </div>
          <div class="col-two form-content bg-light-gray v-middle-inner colleagues-success">
            <div class="form-details v-middle">
              <p>
                Thank you for sharing the power of Eagle Pharma CRM® with your colleagues. They will than you too!
                Shortly, your colleagues will receive an email with further instructions on how to join the Free Trial.
              </p>
              <ul>
                <li class="list-1">Learn how easy it is to collaborate with others in your organization.</li>
                <li class="list-2">Share insights, data discoveries and stories with your peers as a Group.</li>
                <li class="list-3">Maximize the benefits of your trial with your team.</li>
                <li class="list-4">Standerlize the understanding of analytics and get consensus on business much faster.
                </li>
              </ul>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</section>