<section class="col-four-block" id="technical-support">
  <div class="container">
    <div class="intro-block text-center">
      <h3>Resources</h3>
      <p>The Eagle Pharma CRM® application leverages <a href="https://www.qlik.com/us/" target="_blank"
                                                        class="intro-link">Qlik®</a> for analytics and visualization
        purposes. Whether you are new
        to Qlik® or you have some experience with this technology, this section lists some useful resources that can be
        leveraged throughout your free trial.</p>
    </div>


    <div class="column-wrapper">
      <div class="col-two">
        <div class="v-middle-inner">
          <div class="hover v-middle panel featured-box">
            <div class="front bg-lighter-gray">
              <div class="featured-img">
                <div class="svg-icon">
                  <img src="/templates/dist/images/resource-icon1.svg" alt="mobile icon">
                </div>
              </div>
              <div class="featured-text">
                <h5>How to Use Qlik</h5>
                <p>Whether you are new to Qlik or a veteran, this section contains useful links to learn more about
                  Qlik
                  and how to best use most of the features available on the interface while navigating a Qlik
                  application</p>
                <span class="featured-link">More Info</span>
              </div>
            </div>
            <div class="back v-middle bg-curious-blue">
              <div class="featured-img">
                <div class="svg-icon">
                  <img src="/templates/dist/images/resource-icon-white1.svg" alt="mobile icon">
                </div>
              </div>
              <div class="featured-text">
                <h5>How to Use Qlik</h5>
                <ul>
                  <li><a href="https://webapps.qlik.com/associative-difference/staging/index.html" target="_blank">The power of Gray
                      –
                      Qlik Associative Engine </a></li>
                  <li><a href="https://showcase2dev.qlik.com/sense/app/dcaebb91-984d-485c-adb4-d4686bc6c0f6" target="_blank">Enjoy
                      the
                      Associative Experience</a></li>
                  <li><a href="https://www.youtube.com/watch?v=sYZ-7UG6UC8" target="_blank">Getting Started with
                      Qlik
                      Sense</a></li>
                  <li><a href="https://youtu.be/cC5679FE7Dw" target="_blank">Qlik Sense User Interface Tour</a></li>
                  <li><a href="https://qcc.qlik.com/course/view.php?id=951" target="_blank">A Quick Tour of Qlik
                      Sense</a></li>
                  <li><a href="https://www.youtube.com/channel/UCFxZPr8pHfZS0n3jxx74rpA" target="_blank">Qlik Help
                      Channel</a></li>
                  <li><a href="https://community.qlik.com/blogs/qlikproductinnovation" target="_blank">Qlik Product
                      Innovation</a></li>
                </ul>
              </div>
              <span class="close">
                <img src="/templates/dist/images/pricing-details/cross-sign-white.svg" alt="close-btn">
              </span>
            </div>
          </div>
        </div>
      </div>

      <div class="col-two">
        <div class="v-middle-inner">
          <div class="hover panel featured-box">
            <div class="front v-middle bg-lighter-gray">
              <div class="featured-img">
                <div class="svg-icon">
                  <img src="/templates/dist/images/resource-icon2.svg" alt="link icon">
                </div>
              </div>
              <div class="featured-text">
                <h5>How to Upload/Connect to Data</h5>
                <p>This section contains links to learn the basics on how to upload data (your own and/or demo data)
                  into
                  a Qlik application and go from data into insights in just few minutes.</p>
                <span class="featured-link">More Info</span>
              </div>
            </div>
            <div class="back v-middle bg-curious-blue">
              <div class="featured-img">
                <div class="svg-icon">
                  <img src="/templates/dist/images/resource-icon-white2.svg" alt="link icon">
                </div>
              </div>
              <div class="featured-text">
                <h5>How to Upload/Connect to Data</h5>
                <ul>
                  <li><a href="https://community.qlik.com/docs/DOC-6932#dataloading" target="_blank">Data Loading –
                      Getting started </a>
                  </li>
                  <li><a href="https://www.youtube.com/watch?v=lbStCCBzPQI" target="_blank">Qlik Sense Salesforce
                      Connector – Getting
                      Started</a></li>
                </ul>
              </div>
              <span class="close">
                <img src="/templates/dist/images/pricing-details/cross-sign-white.svg" alt="close button">
              </span>
            </div>
          </div>
        </div>
      </div>

      <div class="col-two">
        <div class="v-middle-inner">
          <div class="hover panel featured-box">
            <div class="front v-middle bg-lighter-gray">
              <div class="featured-img">
                <div class="svg-icon">
                  <img src="/templates/dist/images/resource-icon3.svg" alt="box icon">
                </div>
              </div>
              <div class="featured-text">
                <h5>Mobile</h5>
                <p>This section contains links that help you understand the basics of Qlik Sense Mobile and how easy
                  it is
                  to leverage the mobile capabilities as part of your solution. Whether it is online or offline
                  requirements, Qlik Sense Mobile can help you</p>
                <span class="featured-link">More Info</span>
              </div>
            </div>
            <div class="back v-middle bg-curious-blue">
              <div class="featured-img">
                <div class="svg-icon">
                  <img src="/templates/dist/images/resource-icon-white3.svg" alt="box icon">
                </div>
              </div>
              <div class="featured-text">
                <h5>Mobile</h5>
                <ul>
                  <li><a href="https://youtu.be/rPrafeKTF-c" target="_blank">Qlik Sense Mobile – Getting Started</a>
                  </li>
                  <li><a href="https://youtu.be/-QSH4r5lgOk" target="_blank">Mobility with Qlik Sense</a></li>
                  <li><a href="https://community.qlik.com/community/value-added-products/qlik-sense-mobile"
                         target="_blank">Qlik Sense
                      Mobile Community</a></li>
                </ul>
              </div>
              <span class="close">
                <img src="/templates/dist/images/pricing-details/cross-sign-white.svg" alt="close button">
              </span>
            </div>
          </div>
        </div>
      </div>

      <div class="col-two">
        <div class="v-middle-inner">
          <div class="hover panel featured-box">
            <div class="front v-middle bg-lighter-gray">
              <div class="featured-img">
                <div class="svg-icon">
                  <img src="/templates/dist/images/resource-icon4.svg" alt="link icon">
                </div>
              </div>
              <div class="featured-text">
                <h5>FAQs</h5>
                <p>Have any questions? Click here to learn more about the details behind the Free Trial</p>
                <span class="featured-link">More Info</span>
              </div>
            </div>
            <div class="back v-middle bg-curious-blue">
              <div class="featured-img">
                <div class="svg-icon">
                  <img src="/templates/dist/images/resource-icon-white4.svg" alt="link icon">
                </div>
              </div>
              <div class="featured-text">
                <h5>FAQs</h5>
                <ul>
                  <li><a href="" target="_blank">The Power of Gray – Qlik Associative Engine</a></li>
                </ul>
              </div>
              <span class="close">
                <img src="/templates/dist/images/pricing-details/cross-sign-white.svg" alt="close button">
              </span>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</section>