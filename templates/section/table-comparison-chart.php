<section class="solution-comparison">
  <div class="container">
    <div class="intro-block text-center">
      <h3>Solution Comparison Chart</h3>
      <p>Which is the right one for you?</p>
    </div>

    <div class="column-wrapper">
      <table>
        <thead>
        <tr>
          <td class="bg-primary"><h5>Key Features</h5></td>
          <td class="bg-curious-blue"><h5>Custom</h5></td>
          <td class="bg-light-blue"><h5>Power</h5></td>
          <td class="bg-cashmere"><h5>Explorer</h5></td>
        </tr>
        </thead>
        <tbody>
        <tr>
          <td class="for-space"></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td class="features">Access to data for dynamic exploration</td>
          <td>
            <img src="/templates/dist/images/pricing-details/cross-sign.svg" alt="cross icon">
          </td>
          <td>
            <img src="/templates/dist/images/pricing-details/cross-sign.svg" alt="cross icon">
          </td>
          <td>
            <img src="/templates/dist/images/pricing-details/cross-sign.svg" alt="cross icon">
          </td>

        </tr>
        <tr>
          <td class="features">Advanced visualizations</td>
          <td>
            <img src="/templates/dist/images/pricing-details/cross-sign.svg" alt="cross icon">
          </td>
          <td>
            <img src="/templates/dist/images/pricing-details/cross-sign.svg" alt="cross icon">
          </td>
          <td>
            <img src="/templates/dist/images/pricing-details/cross-sign.svg" alt="cross icon">
          </td>
        </tr>
        <tr>
          <td class="features">Sophisticated mapping</td>
          <td>
            <img src="/templates/dist/images/pricing-details/cross-sign.svg" alt="cross icon">
          </td>
          <td>
            <img src="/templates/dist/images/pricing-details/cross-sign.svg" alt="cross icon">
          </td>
          <td>
            <img src="/templates/dist/images/pricing-details/cross-sign.svg" alt="cross icon">
          </td>
        </tr>
        <tr>
          <td class="features">Ability to download to Excel or PowerPoint</td>
          <td>
            <img src="/templates/dist/images/pricing-details/cross-sign.svg" alt="cross icon">
          </td>
          <td>
            <img src="/templates/dist/images/pricing-details/cross-sign.svg" alt="cross icon">
          </td>
          <td>
            <img src="/templates/dist/images/pricing-details/cross-sign.svg" alt="cross icon">
          </td>
        </tr>
        <tr>
          <td class="features">Qlik’s Search & Associative experience</td>
          <td>
            <img src="/templates/dist/images/pricing-details/cross-sign.svg" alt="cross icon">
          </td>
          <td>
            <img src="/templates/dist/images/pricing-details/cross-sign.svg" alt="cross icon">
          </td>
          <td>
            <img src="/templates/dist/images/pricing-details/cross-sign.svg" alt="cross icon">
          </td>
        </tr>
        <tr>
          <td class="features">Mobile device enablement</td>
          <td>
            <img src="/templates/dist/images/pricing-details/cross-sign.svg" alt="cross icon">
          </td>
          <td>
            <img src="/templates/dist/images/pricing-details/cross-sign.svg" alt="cross icon">
          </td>
          <td>
            <img src="/templates/dist/images/pricing-details/cross-sign.svg" alt="cross icon">
          </td>
        </tr>
        <tr>
          <td class="features">Pre-packaged, personalized applications</td>
          <td>
            <img src="/templates/dist/images/pricing-details/cross-sign.svg" alt="cross icon">
          </td>
          <td>
            <img src="/templates/dist/images/pricing-details/cross-sign.svg" alt="cross icon">
          </td>
          <td>
            <img src="/templates/dist/images/pricing-details/cross-sign.svg" alt="cross icon">
          </td>
        </tr>
        <tr>
          <td class="features">Augmented Insights Board</td>
          <td>
            <img src="/templates/dist/images/pricing-details/cross-sign.svg" alt="cross icon">
          </td>
          <td>
            <img src="/templates/dist/images/pricing-details/cross-sign.svg" alt="cross icon">
          </td>
          <td>

          </td>
        </tr>
        <tr>
          <td class="features">Advanced self-service (create & edit dashboards)</td>
          <td>
            <img src="/templates/dist/images/pricing-details/cross-sign.svg" alt="cross icon">
          </td>
          <td>
            <img src="/templates/dist/images/pricing-details/cross-sign.svg" alt="cross icon">
          </td>
          <td>

          </td>
        </tr>
        <tr>
          <td class="features">Story Telling</td>
          <td>
            <img src="/templates/dist/images/pricing-details/cross-sign.svg" alt="cross icon">
          </td>
          <td>
            <img src="/templates/dist/images/pricing-details/cross-sign.svg" alt="cross icon">
          </td>
          <td>

          </td>
        </tr>
        <tr>
          <td class="features">Standardized reporting and distribution</td>
          <td>
            <img src="/templates/dist/images/pricing-details/cross-sign.svg" alt="cross icon">
          </td>
          <td>

          </td>
          <td>

          </td>
        </tr>
        <tr>
          <td class="features">Custom Artificial Intelligence</td>
          <td>
            <img src="/templates/dist/images/pricing-details/cross-sign.svg" alt="cross icon">
          </td>
          <td>

          </td>
          <td>

          </td>
        </tr>
        <tr>
          <td class="features">Mashup Builder</td>
          <td>
            <img src="/templates/dist/images/pricing-details/cross-sign.svg" alt="cross icon">
          </td>
          <td>

          </td>
          <td>

          </td>
        </tr>
        <tr class="for-btn">
          <td class="table-btn">
            <a href="#contact-us" class="btn-default btn-primary">Contact Us to Order</a>
          </td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        </tbody>
      </table>
      <div class="responsive-btn">
        <a href="#contact-us" class="btn-default btn-primary">Contact Us to Order</a>
      </div>
    </div>
  </div>
</section>
