<section class="signup-form text-center colleagues-form thank-you-form">
  <div class="container">
    <div class="intro-block">
      <h3>Thank You for Inviting Colleagues</h3>
    </div>
    <div class="column-wrapper">
      <div class="col-two bg-light-gray v-middle-inner">
        <div class="form-details v-middle">
          <p>Thank you for sharing the power of Sales Analytics with your colleagues. They will thank you too! Shortly,
            your colleagues will receive an email with further instructions on how to join the Free Trial.</p>
          <ul>
            <li class="list-1">Learn how easy it is to collaborate with others in your organization.</li>
            <li class="list-2">Share insights, data discoveries and stories with your peers as a Group.</li>
            <li class="list-3">Maximize the benefits of your trial with your team.</li>
            <li class="list-4">Standerlize the understanding of analytics and get consensus on business much faster.
            </li>
          </ul>
        </div>
      </div>

      <div class="col-two bg-astronaut-blue">
        <h4>Invite Colleagues</h4>
        <p>Enter the emails of the colleagues that you would like to invite to the free trial. All email addresses must
          have the same domain.</p>
        <form action="" netlify id="sign-up-form">
          <input type="email" name="Email_1" placeholder="Email 1">
          <input type="email" name="Email_2" placeholder="Email 2">
          <input type="email" name="Email_3" placeholder="Email 3">
          <input type="email" name="Email_4" placeholder="Email 4">
          <div class="checkbox">
            <input type="checkbox" name="checkbox">
            <label>I agree to all <a href="">Terms & Conditions</a></label>
          </div>
          <div class="g-recaptcha" data-sitekey="6LcWSnUUAAAAAMaA83N8Of0thlFJFT9wLI46q7fc"></div>
          <button type="submit" class="btn-default btn-two">Get the Trial</button>
        </form>
        <div id="sign-up-form-thank-you" style="display: none"></div>
      </div>

    </div>
  </div>
</section>