<section class="hero-slider no-padding">
  <div class="slide-wrapper">
    <div class="slide-item">
      <div class="bg-img">
        <img src="/templates/dist/images/carousel-1.jpg" alt="hero banner">
      </div>
      <div class="v-middle-wrapper">
        <div class="v-middle-inner container text-left">
          <div class="v-middle">
            <div class="hero-content">
              <h2>Eagle Pharma CRM®</h2>
              <p>Empower your sales force with the Eagle Pharma CRM® a data-driven analytics solution to effectively
                engage with a rapidly-evolving healthcare ecosystem</p>
              <a href="#trial" class="inner-link"><span>Sign Up for a Free Trial or Demo</span></a>

              <a href="" data-video=" https://www.youtube.com/embed/sYZ-7UG6UC8"
                 class="play-video"><span>Watch Video</span></a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="slide-item">
      <div class="bg-img">
        <img src="/templates/dist/images/carousel-2.jpg" alt="hero banner">
      </div>
      <div class="v-middle-wrapper">
        <div class="v-middle-inner container text-left">
          <div class="v-middle">
            <div class="hero-content">
              <h2>Analyzing Veeva</h2>
              <p>Enhance Veeva CRM with enterprise analytic capabilities and empower Veeva and non-Veeva users across
                the Commercial organization and beyond by combining Veeva CRM data with Marketing, Finance, Medical
                Information and other data sources to uncover additional insights.</p>
              <a href="#trial" class="inner-link"><span>Sign Up for a Free Trial or Demo</span></a>

              <a href="" data-video=" https://www.youtube.com/embed/sYZ-7UG6UC8"
                 class="play-video"><span>Watch Video</span></a>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="slide-item">
      <div class="bg-img">
        <img src="/templates/dist/images/carousel-3.jpg" alt="hero banner">
      </div>
      <div class="v-middle-wrapper">
        <div class="v-middle-inner container text-left">
          <div class="v-middle">
            <div class="hero-content">
              <h2>Mobile access anytime, anywhere</h2>
              <p>Provide sales leaders and reps with access to interactive information on the go from any mobile device.
                Optimize pre-call planning and time while meeting physician's with Qlik's offline capabilities. </p>
              <a href="#trial" class="inner-link"><span>Sign Up for a Free Trial or Demo</span></a>

              <a href="" data-video=" https://www.youtube.com/embed/sYZ-7UG6UC8"
                 class="play-video"><span>Watch Video</span></a>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="slide-item">
      <div class="bg-img">
        <img src="/templates/dist/images/carousel-4.jpg" alt="hero banner">
      </div>
      <div class="v-middle-wrapper">
        <div class="v-middle-inner container text-left">
          <div class="v-middle">
            <div class="hero-content">
              <h2>The power of geo-analytics</h2>
              <p>Visualize rep coverage and market share by geography for better planning. Plot Key Opinion Leaders to
                understand influence on local, regional and national levels and optimize routes and maximize the number
                of physician meetings possible in a day. </p>
              <a href="#trial" class="inner-link"><span>Sign Up for a Free Trial or Demo</span></a>

              <a href="" data-video=" https://www.youtube.com/embed/sYZ-7UG6UC8"
                 class="play-video"><span>Watch Video</span></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>