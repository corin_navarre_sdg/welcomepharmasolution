<section class="cta-column bg-astronaut-blue" id="about-us">
  <div class="container">
    <div class="intro-block text-center">
      <h3>About Us</h3>
      <p>With offices across United States, Europe and Middle East & Africa, SDG Group is a global systems integrator
        that specializes in business analytics. We create value by empowering our clients to solve the toughest
        challenges in their business. SDG Group provides services and solutions that deliver transformational results
        for a demanding world in this digital era.</p>

      <p class="width-720">We are honored to be recognized by Gartner and Penteo for driving better performance and
        innovation and are
        listed on <a href="http://www.sdggroup.com/en/2018-gartner-market-guide" target="_blank">Gartner’s Market Guide
          for Data and Analytics Service Providers.</a></p>

      <a href="http://www.sdggroup.com/us" class="btn-default learn-more cta_learn_more_sdg" target="_blank">Learn More</a>
    </div>

  </div>
</section>