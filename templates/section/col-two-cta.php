<section class="col-two-cta" id="contact-us">
  <div class="container">
    <div class="intro-block text-center">
      <h3>Need Help?</h3>
    </div>

    <div class="column-wrapper">
      <div class="col-two bg-astronaut-blue">
        <div class="v-middle-inner">
          <div class="v-middle">
            <h4>Technical Support</h4>
              <span class="phone">1 (877) MYSDGUS  |  <a href="tel:1-877-697-3487"> 1 (877) 697-3487 </a> |  Ext.1</span>
            <a href="mailto:contact.usa@sdggroup.com"  class="btn-transparent cta_technical_support">Technical Support</a>
          </div>
        </div>
      </div>
      <div class="col-two bg-primary">
        <div class="v-middle-inner">
          <div class="v-middle">
            <h4>General Inquiries</h4>
            <span class="phone">1 (877) MYSDGUS  |  <a href="tel:1-877-697-3487"> 1 (877) 697-3487 </a> |  Ext.2</span>
            <a href="mailto:contact.usa@sdggroup.com" class="btn-transparent cta_gen_inquires">Buy Now</a>
          </div>
        </div>
      </div>
    </div>

  </div>
</section>