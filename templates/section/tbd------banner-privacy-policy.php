<section class="banner-second-level banner-privacy-policy no-padding">
  <div class="bg-img">
    <img src="/templates/dist/images/pricing-details/banner-second-level-bg.jpg" alt="banner second level">
  </div>
  <div class="v-middle-wrapper">
    <div class="container v-middle-inner">
      <div class="v-middle">
        <h1>Privacy Policy Agreement</h1>
      </div>
    </div>
  </div>
</section>
