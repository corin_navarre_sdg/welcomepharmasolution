<section class="col-two-block text-center">
  <div class="container">
    <div class="intro-block">
      <h3>Eagle Pharma CRM® Demo</h3>
      <p>See what Sales Analytics can do for your business</p>
    </div>
    <div class="column-wrapper">
      <div class="col-two">
        <div class="bg-img">
          <img src="/templates/dist/images/laptop.jpg" alt="laptop">
          <img src="/templates/dist/images/Link_Icon.svg" alt="Link Icon" class="animated infinite pulse link-icon ">
        </div>
      </div>
      <div class="col-two v-middle-inner">
        <div class="v-middle">
          <ul class="category">
            <li>Increase rep efficiency, and accelerate sale cycles</li>
            <li>Leverage the power of Eagle Pharma CRM® anywhere, anytime to meet customers’ needs</li>
            <li>Gain a 360-degree view of all sales activities around specific HCPs</li>
            <li>Optimize planning activities and maximize the impact of time spent with HCPs</li>
          </ul>
          <a href="http://demopharmasolution.sdgeagle.com" class="btn-default cta_launch_demo_middle_page" target="_blank">Launch Demo</a>
          <a href="https://www.youtube.com/embed/sYZ-7UG6UC8" class="btn-two btn-default play-video cta_watch_video">Watch Video</a>
        </div>
      </div>
    </div>
  </div>
</section>