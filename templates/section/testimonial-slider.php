<section class="testimonial-slider no-padding">
  <div class="bg-img overlay-blue">
    <img src="/templates/dist/images/testimonial-bg.jpg" alt="testimonial slider background">
  </div>
  <div class="v-middle-wrapper">
    <div class="v-middle-inner container">
      <div class="v-middle">
        <div class="intro-block white text-center">
          <h3>Testimonials</h3>
        </div>

        <div class="column-wrapper">
            <div class="logo-click">
                <div class="logo-wrap slider-nav">
            <div class="col-four">
              <div class="logo">
                <img src="/templates/dist/images/csl-logo.png" alt="CSL logo">
              </div>
            </div>
            <div class="col-four">
              <div class="logo">
                <img src="/templates/dist/images/almirall-logo.png" alt="Almirall logo">
              </div>
            </div>
          </div>   <!-- customer logo ends here -->
            </div>
            <div class="logo-noclick">
            <div class="col-four">
                <div class="logo">
                    <img src="/templates/dist/images/boehringer-logo.png" alt="Boehringer logo">
                </div>
            </div>
            <div class="col-four">
                <div class="logo">
                    <img src="/templates/dist/images/lundbeck-logo.png" alt="Lundbeck logo">
                </div>
            </div>
            </div>
          <div class="slick-dots"></div>

          <div class="testimonial-wrapper slider-for">
            <div class="testimonial-slide">
              <div class="content-wrap text-center">
                <p>“We went from traditional reporting to self-service to chatbots in 6 months”</p>
                <span>- John Bunn, Director CRM Center of Excellence.  CSL Behring</span>
              </div>
            </div>

            <div class="testimonial-slide">
              <div class="content-wrap text-center">
                <p>“The Qlik technology allows us to easily identify areas for improvement and how to manage the field
                  more efficiently. We can react in real-time.”</p>
                <span>Xavier Martí | BT Business Process Specialist</span>
                <a href="/templates/assets/pdf/CSS_Almirall.pdf"  target="_blank" class="btn-transparent testimonial-btn">View Success Story</a>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</section>