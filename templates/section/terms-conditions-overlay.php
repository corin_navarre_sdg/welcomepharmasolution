<section class="terms-conditions-overlay no-padding-bottom">
    <span class="close">
      <img src="/templates/dist/images/pricing-details/cross-sign.svg" alt="close button">
    </span>
  <?php include "banner-terms-conditions.php"; ?>
  <?php include "terms-conditions-content.php"; ?>

</section>