<section class="qlik-video-overlay no-padding">
    <div class="video-overlay">
        <div class="v-middle-inner">
            <div class="v-middle">
                <div class="video-container">
                    <iframe width="800" height="600" src="about:blank" ></iframe>
                    <button class="closeVideo">
                        <img src="/templates/dist/images/pricing-details/cross-sign.svg" alt="close button">
                    </button>
                </div>
            </div>
        </div>
    </div>
</section>