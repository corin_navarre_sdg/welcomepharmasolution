<section class="sales-analytics">
  <div class="container">
    <div class="intro-block text-center">
      <h3>Eagle Pharma CRM® Pricing</h3>
      <p>Powered by Qlik</p>
    </div>

    <div class="column-wrapper">
      <div class="col-two">
        <div class="table-info bg-primary">
          <h5>Explorers</h5>
          <span>For Analyzer Users</span>
          <p>For start-ups and departments wanting to drive revenue growth and profit.</p>
        </div>
        <div class="table-content bg-light-gray">
          <h6>Time</h6>
          <table>
            <thead>
            <tr>
              <td></td>
              <th id="vol" colspan="5" scope="colgroup">User Volume</th>
            </tr>
            <tr>
              <td></td>
              <th>0-50</th>
              <th>51-150</th>
              <th>151+</th>
            </tr>
            </thead>
            <tbody>
            <tr>
              <th >1 year</th>
              <td>$75</td>
              <td>$65</td>
              <td>$55</td>
            </tr>
            <tr>
              <th>2 years</th>
              <td>$71</td>
              <td>$62</td>
              <td>$52</td>
            </tr>
            <tr>
              <th>3 years</th>
              <td>$68</td>
              <td>$58</td>
              <td>$49</td>
            </tr>
            </tbody>
          </table>
          <h6 class="bottom-heading">Time based discounts for custom packages are based on the scope of the service</h6>
        </div>
      </div>

      <div class="col-two">
        <div class="table-info bg-light-blue">
          <h5>Power</h5>
          <span>For Professional Users</span>
          <p>For Sales Operations and Professionals that want to optimize their CRM and increase their footprint in the market.</p>
        </div>
        <div class="table-content bg-light-gray">
          <h6>Time</h6>
          <table>
            <thead>
            <tr>
              <td></td>
              <th id="vol" class="span" colspan="5" scope="colgroup">User Volume</th>
            </tr>
            <tr>
              <td></td>
              <th>0-50</th>
              <th>51-150</th>
              <th>151+</th>
            </tr>
            </thead>
            <tbody>
            <tr>
              <th>1 year</th>
              <td>$99</td>
              <td>$89</td>
              <td>$79</td>
            </tr>
            <tr>
              <th>2 years</th>
              <td>$94</td>
              <td>$85</td>
              <td>$75</td>
            </tr>
            <tr>
              <th>3 years</th>
              <td>$89</td>
              <td>$80</td>
              <td>$71</td>
            </tr>
            </tbody>
          </table>
          <h6 class="bottom-heading">Time based discounts for custom packages are based on the scope of the service</h6>
        </div>
      </div>
    </div>
  </div>
</section>
