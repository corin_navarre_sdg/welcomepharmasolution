<section class="contact-us no-padding" id="contact-us">
  <div class="bg-img">
    <img src="/templates/dist/images/pricing-details/contact-us-bg.jpg" alt="contact us">
  </div>
  <div class="v-middle-wrapper">
    <div class="container v-middle-inner">
      <div class="v-middle intro-block">
        <h3>Contact Us</h3>
        <p>Our Field Sales Analytics Experts will help you accelerate results</p>
        <div class="contact-details">
          <h5>General Inquiries</h5>
          <span class="phone">1 (877) MYSDGUS  |  1 (877) 697-3487</span>
          <a href="mailto:contact.usa@sdggroup.com" class="btn-transparent">Contact Us</a>
        </div>
      </div>
    </div>
  </div>

</section>