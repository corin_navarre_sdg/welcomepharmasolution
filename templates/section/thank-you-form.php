<section class="signup-form text-center thank-you-form colleagues-form" id="colleagues-form">
  <div class="container">
    <div class="intro-block">
      <h3>Thank You for Signing Up</h3>
    </div>
    <div class="column-wrapper">
      <div class="col-two bg-light-gray v-middle-inner">
        <div class="form-details v-middle">
          <p>You like it? Share it with yours peers and colleagues. Invite up to 4 of your colleagues to join your Free
            Trial and share all the benefits with them. Also, keep an eye on your inbox for an email with your Free
            Trial credentials and detailed instructions on how to access the environment.</p>
          <ul>
            <li class="list-1">Easily collaborate with others in your Organizations with powerful data analytics.</li>
            <li class="list-2">Share insights, data discoveries and stories with your peers as a Group.</li>
            <li class="list-3">Maximize the benefits of your trial with your team.</li>
            <li class="list-4">Standerlize the understanding of analytics and get consensus on business much faster.
            </li>
          </ul>
        </div>
      </div>

      <div class="col-two bg-astronaut-blue">
        <h4>Invite Colleagues</h4>
        <p>Enter the emails of the colleagues that you would like to invite to the free trial. All email addresses must
          have the same domain as yours.</p>
        <form action="" netlify id="sign-up-form">
          <input type="email" name="Email_1" placeholder="Email 1">
          <input type="email" name="Email_2" placeholder="Email 2">
          <input type="email" name="Email_3" placeholder="Email 3">
          <input type="email" name="Email_4" placeholder="Email 4">
          <div class="checkbox">
            <input type="checkbox" name="checkbox">
            <label>I agree to all <a href="">Terms & Conditions</a></label>
          </div>
          <div class="g-recaptcha" data-sitekey="6LcWSnUUAAAAAMaA83N8Of0thlFJFT9wLI46q7fc"></div>
          <button type="submit" class="btn-default btn-two">Invite Colleagues</button>
        </form>
        <div id="sign-up-form-thank-you" style="display: none"></div>
      </div>

    </div>
  </div>
</section>