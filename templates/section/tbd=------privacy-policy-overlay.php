<section class="privacy-policy-overlay no-padding-bottom">
    <span class="close">
      <img src="/templates/dist/images/pricing-details/cross-sign.svg" alt="close button">
    </span>
  <?php include "banner-privacy-policy.php"; ?>
  <?php include "privacy-policy-content.php"; ?>

</section>