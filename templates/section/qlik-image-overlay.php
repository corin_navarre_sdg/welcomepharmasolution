<section class="qlik-image-overlay no-padding">
  <div class="image-overlay">
    <div class="content-wrapper">
      <div class="bg-img">
        <img src="/templates/dist/images/WhatToSeeInDemo_MSPAccelerator.png" alt="Demo Image">
      </div>
      <div class="demo-content-list">
        <h6>Seven Tips on how to navigate the Eagle Pharma CRM® demo.</h6>
        <ol>
          <li>Navigation button to navigate from one sheet to another one is located on the top right corner</li>
          <li>Drop down associative filters to slice and dice all visualizations and charts in the application</li>
          <li>Associative menus (green, white and gray experience) to make selections and understand associations with
            other dimensions very easily
          </li>
          <li>Interactive charts that allow dynamic selections as filters</li>
          <li>Search capabilities on charts to limit the data that is analyzed</li>
          <li>Sorting capabilities on data</li>
          <li>Ability to expand charts and visualizations to personalize visualizations</li>
        </ol>
      </div>
      <span class="close">
      <img src="/templates/dist/images/pricing-details/cross-sign.svg" alt="close button">
    </span>
    </div>
  </div>
</section>