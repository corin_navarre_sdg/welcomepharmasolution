'use strict'
var app = angular.module('qlikApp', ['ngAnimate', 'ngMessages']);

app.directive('exemptMail', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attr, ctrl) {
            function customValidator(ngModelValue) {
                var exemptEmail = ['gmail.com', 'hotmail.com', 'outlook.com','yahoo.com'];
                if(ngModelValue) {
                    var curemail = ngModelValue.replace(/.*@/, "");
                    var emailindex = exemptEmail.indexOf(curemail);
                    if(emailindex >=0) {
                      var emailFlag = true;
                    }else {
                        var emailFlag = false;
                    }
                }
                if (!emailFlag) {
                    ctrl.$setValidity('workmail', true);
                } else {
                    ctrl.$setValidity('workmail', false);
                }
                if (/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/.test(ngModelValue)) {
                    ctrl.$setValidity('validmail', true);
                } else {
                    ctrl.$setValidity('validmail', false);
                }
                if (ngModelValue.length > 0) {
                    ctrl.$setValidity('emailrequired', true);
                } else {
                    ctrl.$setValidity('emailrequired', false);
                }
                return ngModelValue;
            }
            ctrl.$parsers.push(customValidator);
        }
    };
});


app.directive('colleaguesMail', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attr, ctrl) {
            function customValidator(ngModelValue) {
                var signup_email = getCookie("signupemail");
                var usemail_domian = signup_email.replace(/.*@/, "");
                var exemptEmail = ['gmail.com', 'hotmail.com', 'outlook.com','yahoo.com'];

                if(ngModelValue) {
                    var curemail = ngModelValue.replace(/.*@/, "");
                    var emailindex = exemptEmail.indexOf(curemail);
                    if(emailindex >=0) {
                        var emailFlag = true;
                    }else {
                        var emailFlag = false;
                    }
                }
                if(ngModelValue) {
                    if (usemail_domian == curemail) {
                        ctrl.$setValidity('businessmail', true);
                    } else {
                        ctrl.$setValidity('businessmail', false);
                    }
                    if (!emailFlag) {
                        ctrl.$setValidity('workmail', true);
                    } else {
                        ctrl.$setValidity('workmail', false);
                    }
                    if (/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/.test(ngModelValue)) {
                        ctrl.$setValidity('validmail', true);
                    } else {
                        ctrl.$setValidity('validmail', false);
                    }
                   if (ngModelValue.length > 0) {
                        ctrl.$setValidity('emailrequired', true);
                    } else {
                        ctrl.$setValidity('emailrequired', false);
                    }
                } else {
                    ctrl.$setValidity('businessmail', true);
                    ctrl.$setValidity('workmail', true);
                    ctrl.$setValidity('validmail', true);
                    ctrl.$setValidity('emailrequired', true);
                }
                if((ngModelValue.length > 0)) {
                    scope.emailarr =[scope.email1, scope.email2, scope.email3, scope.email4]
                    var matchindex = scope.emailarr.indexOf(ngModelValue);
                    if(matchindex >=0) {
                        var emailmatch = true;
                    }else {
                        var emailmatch = false;
                    }
                    if(!emailmatch) {
                        ctrl.$setValidity('emailmatch', true);
                    }else {
                        ctrl.$setValidity('emailmatch', false);
                    }
                } else {
                    ctrl.$setValidity('emailmatch', true);
                }
                return ngModelValue;
            }
            ctrl.$parsers.push(customValidator);
        }
    };
});

app.directive('colleaguesTerms', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attr, ctrl) {
            function customValidator(ngModelValue) {
                if (ngModelValue) {
                    ctrl.$setValidity('termsrequired', true);
                } else {
                    ctrl.$setValidity('termsrequired', false);
                }
                return ngModelValue;
            }
            ctrl.$parsers.push(customValidator);
        }
    };
});


function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}