
(function (jq) {
  // On DOM ready

  //Image responsive code for all browsers

  jq.fn.imageResponsive = function () {
    return this.each(function () {
      var img = jq(this).find("img"),
        defaultWidth = img.prop('naturalWidth'),
        defaultHeight = img.prop('naturalHeight'),
        parentHeight = jq(this).outerHeight(true),
        parentWidth = jq(this).width(),
        aspectRatio = defaultWidth / defaultHeight;
      img.css({
        "height": "auto",
        "width": "100%",
        "margin-left": "0px",
        "max-width": "inherit"
      });
      var imgHeight = parentWidth / aspectRatio;
      var imgTop = (imgHeight - parentHeight) / 2;
      img.css({
        "margin-top": "-" + imgTop + "px"
      });
      if (img.height() < parentHeight) {
        img.css({
          "height": "100%",
          "width": "auto"
        });
        var right_margin = (img.width() - parentWidth) / 2;
        img.css({
          "margin-left": "-" + right_margin + "px",
          "margin-top": "0"
        });
      }
      else if (img.width() < parentWidth) {
        img.css({
          "height": "auto",
          "width": "100%",
          "margin-left": "0"
        });
        img.css({
          "margin-top": "-" + imgTop + "px"
        });
      }
    });
  };

  $(window).on('load', function(){
      var currentLocation = window.location;
      var hashUrl = currentLocation.hash;
      if(hashUrl) {
          $('html, body').animate({scrollTop: $(hashUrl).offset().top - 65}, 1000);
      }
      $('a.inner-link').on('click', function() {
          $('html, body').animate({scrollTop: $(this.hash).offset().top - 65}, 500);
          return false;
      });
  });

})(jQuery);