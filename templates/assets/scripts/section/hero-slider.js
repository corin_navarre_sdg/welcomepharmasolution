// Use $ instead of $uery without replacing global $
(function ($) {
  //On DOM ready
  $(document).ready(function () {
    $('.hero-slider .slide-wrapper').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      dots: true,
      infinite: true,
      draggable: true,
      arrows: false,
      autoplay: true,
      autoplaySpeed: 6000,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            infinite: true,
            draggable: true,
            arrows: false
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            infinite: true,
            draggable: true,
            arrows: false
          }
        },
        {
          breakpoint: 576,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            infinite: true,
            draggable: true,
            arrows: false
          }
        }
      ]
    });

  });


  $(window).on('load resize', function () {
    var $header_height = $('header').outerHeight();
    $('.hero-slider').css({'margin-top': $header_height + 'px'});
  });

    $(window).on('load', function () {
      $('.hero-slider  .hero-content, .hero-slider .bg-img').css({'opacity' :'1'});
    });
})(jQuery);