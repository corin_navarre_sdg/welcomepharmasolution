// Use $ instead of $uery without replacing global $
(function ($) {
  // On DOM ready

  function delete_cookie(name) {
    document.cookie = name + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
  }

  function setCookie(cname, cvalue, email) {
    document.cookie = cname + "=" + cvalue + ";" + ";path=/";
    document.cookie = 'signupemail' + "=" + email + ";" + ";path=/";
  }

  function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }

  function checkCookie() {
    var signup_val = getCookie("signup");
    if (signup_val != '') {
      var $formHeight = $('#colleagues-form').outerHeight();
      $('.signup-form .form-wrap').css({'height': $formHeight + 'px'});
      $(".signup-form-inner").hide();
      $("#colleagues-form").show();
      $('#invite_colleagues input[name=group_id]').val(signup_val);
    }
  }

  /* Generate random group id and append in form */
  function makeid() {
    var curDate = new Date() / 1000 | 0;
    var uniquestr = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < 6; i++)
      uniquestr += possible.charAt(Math.floor(Math.random() * possible.length));
    var groupId = curDate + uniquestr;
    $("#sign-up-form input[name=group_id]").val(groupId);
  }

  $(document).ready(function () {
    makeid();
    checkCookie();
    var signup_val = getCookie("signup");
    if (!signup_val) {
      delete_cookie('signupemail');
    }
    $("#sign-up-form").submit(function (e) {
      e.preventDefault();
      var v = grecaptcha.getResponse();
      var $form = $(this);
      var id = $("#sign-up-form input[name=group_id]").val();
      var email = $('#sign-up-form').find('input[name="email"]').val();
      if (v.length == 0) {
        e.preventDefault();
        $('.g-recaptcha-err').html("You can't proceed! Please check reCAPTCHA to proceed.");
      } else {
        $.post($form.attr("action"), $form.serialize()).then(function () {
          var $formHeight = $('#colleagues-form').outerHeight();
          $('.signup-form .form-wrap').css({'height': $formHeight + 'px'});
          $(".signup-form-inner").hide();
          $("#colleagues-form").show();
          $('#invite_colleagues input[name=group_id]').val(id);
          setCookie("signup", id, email);
          grecaptcha.reset();
          $('.g-recaptcha-err').empty();
        });
      }
    });

    $("#invite_colleagues").submit(function (e) {
      e.preventDefault();
      var $form = $(this);
      var id = $("#invite_colleagues input[name=group_id]").val();
      $.post($form.attr("action"), $form.serialize()).then(function () {
        $('#colleagues-form h3').empty().append('Thank You for Inviting Colleagues');
        $('#colleagues-form .column-wrapper > .col-two.bg-light-gray').not('.bg-light-gray.colleagues-success').hide();
        $('#colleagues-form .bg-light-gray.colleagues-success').show();
        delete_cookie('signup');
        $('#invite_colleagues input[name=group_id]').val(id);
        $('#invite_colleagues').each(function () {
          this.reset();
        });
      });
    });
  });

  $(window).on('load resize', function () {
    if ($(window).width() > 596) {
      $('.signup-form .col-two .bg-img ').imageResponsive();
    } else {
      $('.signup-form .col-two .bg-img img').removeAttr("style");
    }
  });
  $(document).ready(function () {
    $('.signup-form .column-wrapper .col-two').matchHeight();
  });


})(jQuery);

