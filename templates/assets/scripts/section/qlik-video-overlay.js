(function ($) {
    // On DOM ready
    $(function () {
        videoOverlay();
        video_height();
    });

    $(window).on("load scroll resize", function () {
        video_height();
    });

    /*
    * Video overlay
    */
    function videoOverlay() {
        jQuery(document).on('click', '.play-video', function (e) {
            e.preventDefault();
            e.stopPropagation();
            jQuery('body').addClass('stop-scroll');
            jQuery('.qlik-video-overlay').fadeIn();
            jQuery(".video-overlay").addClass("we-active");
            var url = jQuery(this).attr('data-video');
            if (typeof url === 'undefined')
                url = jQuery(this).attr('href');

            // For smooth transition on scaling effect
            jQuery('.video-overlay.we-active').find('iframe').attr('src', url + '?autoplay=1');

        });
        closeVideoOverlay();
    }

    function closeVideoOverlay() {
        jQuery('.close-video').on('click', function () {
            jQuery('.qlik-video-overlay').fadeOut();
            jQuery('.video-overlay').find('iframe').attr('src', '');
            jQuery('body').removeClass('stop-scroll');
            jQuery('.video-overlay').removeClass("we-active");
        });

        // Close video overlay on outside click
        jQuery('.v-middle-inner').on('click', function () {
            jQuery('.qlik-video-overlay').fadeOut();
            jQuery('.video-overlay').find('iframe').attr('src', '');
            jQuery('body').removeClass('stop-scroll');
            jQuery('.video-overlay').removeClass("we-active");
        });

        // Prevent closing on video's click
        jQuery('.video-overlay .v-middle-inner iframe').on('click', function (e) {
            e.stopPropagation();
        });
    }

    function video_height() {
        var windowheight = jQuery(window).height() / 1.9,
            windowWidth = jQuery(window).width() / 1.5,
            imageWidth = jQuery(window).width();
        jQuery('.video-overlay iframe ').attr('width', windowWidth + 'px');
        jQuery('.video-overlay iframe').attr('height', windowheight + 'px');
    }

})(jQuery);