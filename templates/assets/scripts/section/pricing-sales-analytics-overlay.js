(function ($) {
  // On DOM ready
  $(function () {
    $('.sales-analytics .table-info > span').click(function () {
      $('.qlik-pricing-overlay').fadeIn();
    });
    $('.qlik-pricing-overlay .close').click(function () {
      $('.qlik-pricing-overlay').fadeOut();
    });

    // $(".qlik-pricing-overlay a").on('click', function (event) {
    //   $('.qlik-pricing-overlay').fadeOut();
    //   $('.pricing-overlay').fadeOut();
    //   $('body').removeClass('pricing-overlay-active');
    //   if (this.hash !== "") {
    //     event.preventDefault();
    //     var hash = this.hash;
    //     $('html, body').animate({
    //       scrollTop: ($(hash).offset().top)
    //     }, 800);
    //   }
    // });

    $('.qlik-pricing-overlay .content-wrapper').click(function (e) {
      e.stopPropagation();
    });
    $('.pricing-sales-analytics-overlay').click(function () {
      $('.qlik-pricing-overlay').fadeOut();
    });
  });

  //
  // $(window).on('load resize', function () {
  //   var windowWidth = $(window).width();
  //   var $header_height = $('header').outerHeight();
  //
  //   // if (windowWidth < 992) {
  //   //   $('.pricing-sales-analytics-overlay').css({'top': $header_height + 10 + 'px'});
  //   // } else {
  //   //   $('.pricing-sales-analytics-overlay').removeAttr('style');
  //   // }
  // });

  $(document).ready(function () {
    $('.qlik-pricing-overlay .col-two').matchHeight();
    $('.qlik-pricing-overlay .col-two .feature-details').matchHeight();
  });

})(jQuery);