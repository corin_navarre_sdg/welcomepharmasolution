// Use $ instead of $uery without replacing global $
(function ($) {
  // On DOM ready
  $(document).ready(function () {
    $('.slider-nav').slick({
      slidesToShow: 4,
      slidesToScroll: 1,
      asNavFor: '.slider-for',
      dots: true,
      arrows: false,
      centerMode: false,
      focusOnSelect: true,
      draggable: false,
      responsive: [
        {
          breakpoint: 992,
          settings: {
            centerPadding: '0',
            slidesToShow: 3,
            slidesToScroll: 1,
            infinite: true,
            dots: false
          }
        },
        {
          breakpoint: 768,
          settings: {
            centerPadding: '60px',
            slidesToShow: 1,
            dots: false
          }
        },
        {
          breakpoint: 480,
          settings: {
            centerPadding: '15px',
            slidesToShow: 1,
            dots: false
          }
        }
      ]


    });
    $('.slider-for').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
      fade: false,
      asNavFor: '.slider-nav'
    });


    $('.testimonial-slider .testimonial-wrapper .slick-slide').matchHeight();

  });

  // $(document).ready(function(){
  //   $('.testimonial-slider .slick-next').click(function(){
  //       var curr_index = $('.slick-current.slick-active').attr('data-slick-index');
  //       console.log(curr_index);
  //       $('.slider-nav').slick('slickGoTo', slideno - 1);
  //   });
  // });

  $(window).on('load resize', function () {
    $('.testimonial-slider .bg-img ').imageResponsive();
  });
    $(window).on('load', function () {
        $('.testimonial-slider .column-wrapper').css({'opacity' :'1'});
    });

})(jQuery);



