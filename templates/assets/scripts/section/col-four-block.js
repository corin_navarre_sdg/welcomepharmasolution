// Use $ instead of $uery without replacing global $
(function ($) {
  // On DOM ready
    $(document).ready(function () {
     // $('.col-four-block .col-two .v-middle-inner').matchHeight();
   $('.col-four-block .col-two .front').matchHeight();

    $(".col-four-block .col-two .hover .back .close").on('click', function (e) {
      e.preventDefault();
      e.stopPropagation();
        if($('html').hasClass('ua-ie-11')){
            $('.col-four-block .col-two .hover').removeClass("ie-flip");
        }else {
            $('.col-four-block .col-two .hover').removeClass("flip");
        }

    });
    $(".col-four-block .col-two .featured-text span").on('click', function (e) {
      e.preventDefault();
      e.stopPropagation();
        if($('html').hasClass('ua-ie-11')){
            $('.col-four-block .col-two .hover').removeClass("ie-flip");
            $(this).parent().parent().parent().addClass("ie-flip");
        } else{
            $('.col-four-block .col-two .hover').removeClass("flip");
            $(this).parent().parent().parent().addClass("flip");
        }
    });

    $(".col-four-block .col-two .hover").on('click', function (e) {
      e.stopPropagation();
    });
    $(document).on('click',function(){
        if($('html').hasClass('ua-ie-11')){
            $('.col-four-block .col-two .hover').removeClass("ie-flip");
        }else {
            $('.col-four-block .col-two .hover').removeClass("flip");
        }
    });

  });
})(jQuery);