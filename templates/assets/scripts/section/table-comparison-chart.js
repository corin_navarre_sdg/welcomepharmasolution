// Use $ instead of $uery without replacing global $
(function ($) {
  // On DOM ready

  $(function () {
    var $window_width = $(window).width();
    var tableElement = $('.solution-comparison table thead tr > td').length;
    var tableElementLength = $('.solution-comparison table tbody tr').length;
    if ($window_width <= 480) {
      $table_responsive($window_width, tableElement);
    }
  });

  function $table_responsive(tableElement) {

    // Add match Height for Element
    for (var i = 0; i <= tableElement; i++) {
      $('.solution-comparison table tr > td:nth-child(' + i + ')').matchHeight({
        byRow: false,
      });
    }

  }

  $(window).resize(function () {
    var $window_width = $(window).width();
    var tableElement = $('.solution-comparison table thead tr > td').length;
    var tableElementLength = $('.solution-comparison table tbody tr').length;
    if ($window_width <= 480) {
      $table_responsive(tableElement);
    }
  })

})(jQuery);


