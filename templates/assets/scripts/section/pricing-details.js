// Use $ instead of $uery without replacing global $
(function ($) {
  // On DOM ready


  $(document).ready(function () {
    setTimeout(function () {
      $('.pricing-details .col-four').matchHeight();
      $('.pricing-details .col-four .hover .front').matchHeight();
    }, 800);
  });

  $(document).ready(function () {
    var $window_width = $(window).width();
    if ($window_width <= 768) {
      $('.pricing-details .col-four .hover .back .content-text').matchHeight();
    }

    $(".pricing-details .col-four .front .price-link a").on('click', function (e) {
      e.preventDefault();
      e.stopPropagation();
        if($('html').hasClass('ua-ie-11')){
            $('.pricing-details .col-four .hover').removeClass("ie-flip");
            $(this).parent().parent().parent().parent().addClass("ie-flip");
        } else{
            $('.pricing-details .col-four .hover').removeClass("flip");
            $(this).parent().parent().parent().parent().addClass("flip");
        }
    });
    $(".pricing-details .col-four .hover .back .close").on('click', function (e) {
      e.preventDefault();
      e.stopPropagation();
        if($('html').hasClass('ua-ie-11')){
            $('.pricing-details .col-four .hover').removeClass("ie-flip");
        }else {
            $('.pricing-details .col-four .hover').removeClass("flip");
        }

    });
      $(".pricing-details .col-four .hover").on('click', function (e) {
      e.stopPropagation();
    });
    $(document).on('click', function () {
        if($('html').hasClass('ua-ie-11')){
            $('.pricing-details .col-four .hover').removeClass("ie-flip");
        }else {
            $('.pricing-details .col-four .hover').removeClass("flip");
        }
    });

  });
})(jQuery);