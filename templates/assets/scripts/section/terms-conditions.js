// Use $ instead of $uery without replacing global $
(function ($) {
    // On DOM ready
    $(document).ready(function () {
        /* terms-conditions Overlay on click*/

        $('footer .bottom-links .terms-conditions-link, .checkbox .terms-conditions-link').click(function(e){
            e.preventDefault();
            $('.terms-conditions-overlay').fadeIn();
            $('body').addClass('terms-conditions-overlay-active');
        });
      $('.terms-conditions-overlay .close').click(function(){
        $('body').removeClass('terms-conditions-overlay-active');
        $('.terms-conditions-overlay').fadeOut();
      });
    });




})(jQuery);