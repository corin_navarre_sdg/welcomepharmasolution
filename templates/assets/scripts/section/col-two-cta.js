// Use $ instead of $uery without replacing global $
(function ($) {
  // On DOM ready
  $(window).on('load resize', function () {
    var windowSize = $(window).width();
    if (windowSize < 480) {
      $('.col-two-cta .col-two').css('height','auto');
    } else if (windowSize < 767) {
      $('.col-two-cta .col-two').matchHeight();
    }
  });
})(jQuery);