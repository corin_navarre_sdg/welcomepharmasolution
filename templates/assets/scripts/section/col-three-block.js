// Use $ instead of $uery without replacing global $
(function ($) {
  // On DOM ready
  $(document).ready(function () {
    $('.col-three-block .col-three .content-head ').matchHeight();
    $('.col-three-block .col-three .content-text ').matchHeight();
  });


})(jQuery);