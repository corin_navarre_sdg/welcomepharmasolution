// Use $ instead of $uery without replacing global $
(function ($) {
    // On DOM ready
    $(document).ready(function () {
        /* terms-conditions Overlay on click*/

        $('footer .bottom-links .privacy-policy-link, .checkbox .privacy-policy-link').click(function(e){
            e.preventDefault();
            $('.privacy-policy-overlay').fadeIn();
            $('body').addClass('privacy-policy-overlay-active');
        });
      $('.privacy-policy-overlay .close').click(function(){
        $('body').removeClass('privacy-policy-overlay-active');
        $('.privacy-policy-overlay').fadeOut();
      });
    });




})(jQuery);