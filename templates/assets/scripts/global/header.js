// Use $ instead of $uery without replacing global $
(function ($) {
  // On DOM ready
        $(function(){
            var siteName = window.location.host.replace('www.', '');
            $('a[href^="htt"]:not([target]):not([href*="www.' + siteName + '"]):not([href*="//' + siteName + '"]):not([class*="fancybox"])').attr('target', '_blank');
        });
  $(function () {
    // Code starts from here
    $('.toggle').on('click', function () {

      $(this).toggleClass('active');
      if ($('nav .toggle').hasClass('active')) {
        $('nav .main-menu').slideDown();
      } else {
        $('nav .main-menu').slideUp();
      }
    });

    var $winWidth = $(window).width();
      if($winWidth < 768) {
          $('header .main-nav').addClass('mobile-menu');
      } else{
          $('header .main-nav').removeClass('mobile-menu');
      }
          $('.mobile-menu .main-menu li a').click(function(){
          $('nav .main-menu').slideUp();
      });
      $('nav .main-menu li a').click(function(){
          $('nav .toggle').removeClass('active');
      })
//dropdown function
    var clickAble = $('nav > ul > .dropdown');
    $(clickAble).click(function (e) {

      $(this).toggleClass('active');
      $(this).siblings().removeClass('active');
      if ($(this).hasClass('active')) {
        $(this).find('.sub-menu').slideDown();
      } else {
        $(this).find('.sub-menu').slideUp();
      }
      $(clickAble).find(".sub-menu").not($(this).find(".sub-menu")).slideUp();
    });

  });
    $(window).resize(function(){
        var $winWidth = $(window).width();
        if($winWidth < 768) {
            $('header .main-nav').addClass('mobile-menu');
        } else{
            $('header .main-nav').removeClass('mobile-menu');
        }
        $('.mobile-menu .main-menu li a').click(function(){
            $('nav .main-menu').slideUp();
        });
    });
})(jQuery);