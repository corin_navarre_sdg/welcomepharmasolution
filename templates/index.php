<?php

/*
*Utility functions
*/
function we_get_json(){
  if(isset($_GET['page'])){
    $page = $_GET['page'];
  }else{
    $page = 'list-of-pages';
  }

  $file_path = "assets/data/".$page.".json";
  if(!file_exists($file_path)){
    $file_path = "assets/data/404.json";
  }
  $json = file_get_contents($file_path);
  return json_decode($json, true);
}

function we_print_section($key, $items){

  if(strpos($key, "_copy")){
    $key = substr($key, 0, strpos($key, "_copy"));
  }
  $section = 'section/'.$key.'.php';
  if(file_exists($section)){
    include $section;
  }
}


    include_once 'common/header.php';


?>
<?php

/*
* get json based on url
*/
$json_array = we_get_json();

/*
* Include section php based on keys in json
*/
foreach ($json_array as $key => $items){
  we_print_section($key, $items);
}

    include_once 'common/footer.php';

