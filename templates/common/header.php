<!DOCTYPE html >
<html lang="en-US">
<head>
  <title>SDG Group | Eagle Pharma CRM®</title>
  <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">
    <meta name="format-detection" content="telephone=no">
    <meta name="description" content="Free trial - Empower your sales with the Eagle Pharma CRM® a data-driven analytics solution to effectively engage with a rapidly-evolving healthcare ecosystem."/>
    <link rel="stylesheet" href="/templates/dist/styles/global.css"/>
    <link rel="stylesheet" href="/templates/dist/styles/main.css"/>
    <link rel="stylesheet" href="https://use.typekit.net/fsu1mtr.css">
    <script src="/templates/dist/scripts/scripts.js"></script>
    <script type="text/javascript">
        $(window).on('load', function () {
            $("#loader-wrapper").fadeOut(100);
            setTimeout(function(){
                $(".main-wrap").css({'opacity' :'1'});
            }, 50);
        });
    </script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-127527013-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-127527013-1');
    </script>
    <script type="text/javascript">
        _linkedin_partner_id = "537065";
        window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || [];
        window._linkedin_data_partner_ids.push(_linkedin_partner_id);
    </script><script type="text/javascript">
        (function(){var s = document.getElementsByTagName("script")[0];
            var b = document.createElement("script");
            b.type = "text/javascript";b.async = true;
            b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
            s.parentNode.insertBefore(b, s);})();
    </script>
    <noscript>
        <img height="1" width="1" style="display:none;" alt="" src="https://dc.ads.linkedin.com/collect/?pid=537065&fmt=gif" />
    </noscript>
</head>
<body class="ql-18" ng-app="qlikApp">
<div id="loader-wrapper">
    <div id="loader"></div>
</div>
<div class="main-wrap">
<header>
  <nav class="main-nav bg-lighter-gray fixed-top">
    <div class="container">
      <div class="toggle">
        <span></span>
        <span></span>
        <span></span>
      </div>
      <div class="logo">
        <a href="/">
          <img src="/templates/dist/images/sdg-logo.svg" class="" alt="SDG Logo">
        </a>
      </div>
      <ul class="main-menu">
        <li>
          <a href="#solution" data-letters="Products" class="inner-link">Solution</a>
        </li>
        <li>
          <a href="#trial" data-letters="solutions" class="inner-link">Trial</a>
        </li>

        <li class="pricing">
          <a href="pricing.html" data-letters="customers">Pricing</a>
        </li>
        <li>
          <a href="#technical-support" data-letters="Partners" class="inner-link">Technical Support</a>
        </li>
        <li>
          <a href="#about-us"  data-letters="Company" class="inner-link">About Us</a>
        </li>
        <li class="mobile-button">
          <a href="#contact-us" class="inner-link cta_contact_us">Buy Now</a>
        </li>
      </ul>
      <div class="contact-us">
        <a href="#contact-us" class="btn-default gray-btn inner-link cta_contact_us">Buy Now</a>
      </div>
    </div>
  </nav>
</header>