### Local Setup
Localhost: qlik.test

npm install

gulp build

### Dev Site






### Semantic HTML5 Elements:

< header>

< nav>

< section>

< footer>

List of html5 tags https://developer.mozilla.org/en-US/docs/Web/HTML/Element

Basic tags like h, p, a, ul li, ol li, table and others should not need any class to set default style.

### Links And Images
Link images, js and css files from /templates/dist folder

No hardcoded external, local or dev links

Resize and optimize images.

Use SVGs when possible. All image elements should support SVGs.

Use PNG images only if transparent.


### Browser And Device Support
Browsers: Chrome, Firefox, Safari (MacOS), Edge, IE11, and IE10

Devices: iPad, iPhone 7, iPhone 7 Plus, iPhone SE, Galaxy S8 (v7), Nexus 7 (v6)

Use Safari on iOS 11 for Apple devices and Chrome on Android devices




### Code Tested On:
php 7.1.1

node --version

v6.10.1

gulp --version

CLI version 3.9.1

bower --version

1.8.2


